# Infodemics (Indonesia) by [KawalCovid19.id](http://kawalcovid19.id)

## Followed Us:

*  [Instagram](https://www.instagram.com/kawalcovid19.id/)

*  [Twitter](https://twitter.com/KawalCOVID19)

*  [Facebook](https://www.facebook.com/KawalCOVID19)

## Terms of Use:
This Gitlab repo and its contents herein, including all data, mapping, and analysis are owned by respective Local Government of Indonesia, all rights reserved, is provided to the public strictly for educational and academic research purposes. The Website relies upon publicly available data from multiple sources, that do not always agree. [KawalCOVID19.id](http://kawalcovid19.id) hereby disclaims any and all representations and warranties with respect to the Website, including accuracy, fitness for use, and merchantability. Reliance on the Website for medical guidance or use of the Website in commerce is strictly prohibited.

## Copyright
[KawalCOVID19.id](http://kawalcovid19.id) disebarluaskan di bawah [Lisensi Creative Commons Atribusi-NonKomersial-TanpaTurunan 4.0 Internasional](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.id)