# 2019 Novel Coronavirus COVID-19 (2019-nCoV) Data Repository of [Provinsi Jawa Barat](https://jabarprov.go.id/) by [KawalCovid19.id](http://kawalcovid19.id)
This is the data repository for the 2019 Novel Coronavirus which hosts Covid-19 reports Pemerintah Daerah Jawa Barat collected and analyzed by [KawalCovid19.id](http://kawalcovid19.id)

Raw data is standardize to make data uniform in order to drive information into a database and record it there in a consistent, predictable, and homogenous way. This common format enables collaborative research, large-scale analytics, and sharing of sophisticated tools and methodologies. With more consistent data, it becomes easier, faster, and more cost-efficient to analyze data, find trends, target, nurture, and convert.


## Last Update
19 April 2020 22:00 GMT +1

## District-Level (Kabupaten) Data in Time-Series
Filename: time_series_covid19_uniform_jabar_districts.csv

Columns name is standardized and added calculated columns for easy plotting. All records are district-level on specified date

| Columns           | Description    |
| ---------------- |----------------|
| date | The date of this record |
| prov_id | Province ID |
| province | Province name |
| district_id | District ID |
| district | District name (Kabupaten / Kota) |
| diff_odp | ODP changes in this date. It could be positive, negative or zero |
| diff_pdp | PDP changes in this date. It could be positive, negative or zero  |	
| new_cases | New cases added in this date. Zero or more |	
| new_recovered | New recovered added in this date. Zero or more |	
| new_deaths | New deaths added in this date. Zero or more |	
| cases | Total cummulative cases as per date |	
| recovered | Total cummulative recovered as per date |	
| deaths | Total cummulative deaths as per date |	
| prev_cases | Total cases as per (date-1) or `cases` - `new_cases` |	
| prev_recovered | Total recovered as per (date-1) or `recovered` - `new_recovered` |	
| prev_deaths | Total deaths as per (date-1) or `deaths` - `new_deaths` |	
| odp | Total odp as per date |	
| pdp | Total pdp as per date |	
| active_cases | Total active cases which are `cases` - (`recovered` + `deaths`) |	
| prev_odp | Total odp as per (date-1) or `odp` - `diff_odp` |	
| prev_pdp | Total pdp as per (date-1) or `pdp` - `diff_pdp` |	
| %_growth_cases | % growth rate of cases which are `new_cases` / `prev_cases` |
| %_growth_recovered | % growth rate of recovered which are `new_recovered` / `prev_recovered` |
| %_growth_deaths | % growth rate of deaths which are `new_deaths` / `prev_deaths` |

## Province-Level Data in Time-Series
Filename: time_series_covid19_uniform_jabar_province.csv

Columns name is standardized and added calculated columns for easy plotting. All records are province-level (aggregated from all districts) on specified date

| Columns           | Description    |
| ---------------- |----------------|
| date | The date of this record |
| prov_id | Province ID |
| province | Province name |
| diff_odp | ODP changes in this date. It could be positive, negative or zero |
| diff_pdp | PDP changes in this date. It could be positive, negative or zero  |	
| new_cases | New cases added in this date. Zero or more |	
| new_recovered | New recovered added in this date. Zero or more |	
| new_deaths | New deaths added in this date. Zero or more |	
| cases | Total cummulative cases as per date |	
| recovered | Total cummulative recovered as per date |	
| deaths | Total cummulative deaths as per date |		
| prev_cases | Total cases as per (date-1) or `cases` - `new_cases` |	
| prev_recovered | Total recovered as per (date-1) or `recovered` - `new_recovered` |	
| prev_deaths | Total deaths as per (date-1) or `deaths` - `new_deaths` |	
| odp | Total odp as per date |	
| pdp | Total pdp as per date |	
| active_cases | Total active cases which are `cases` - (`recovered` + `deaths`) |	
| prev_odp | Total odp as per (date-1) or `odp` - `diff_odp` |	
| prev_pdp | Total pdp as per (date-1) or `pdp` - `diff_pdp` |	
| %_growth_cases | % growth rate of cases which are `new_cases` / `prev_cases` |
| %_growth_recovered | % growth rate of recovered which are `new_recovered` / `prev_recovered` |
| %_growth_deaths | % growth rate of deaths which are `new_deaths` / `prev_deaths` |

## Province-Level with Normalized Date Started From 100th Cases
Filename: tnorm-cases_covid19_uniform_jabar_province.csv

Rows are filtered when `cases` reached 100th cases. This dataset is useful to plot in logaritmic scale to find doubling days with `active_days` as the x-axis 

| Columns           | Description    |
| ---------------- |----------------|
| date | The date of this record |
| province | Province name |
| cases | Total cases as per date |	
| first_reported | First date when reached 100th cases |	
| active_days | No. days since reach 100th cases |	
| prev_cases | Total cases as per (date-1) or `cases` - `new_cases` |	
| new_cases | New cases added in this date. Zero or more |	
| %_growth_cases | % growth rate of cases which are `new_cases` / `prev_cases` |


## Data Sources:
Pemerintah Daerah Jawa Barat: https://pikobar.jabarprov.go.id/

## Followed Us:

*  [Instagram](https://www.instagram.com/kawalcovid19.id/)

*  [Twitter](https://twitter.com/KawalCOVID19)

*  [Facebook](https://www.facebook.com/KawalCOVID19)

## Terms of Use:
This Gitlab repo and its contents herein, including all data, mapping, and analysis are owned by respective Local Government of Indonesia, all rights reserved, is provided to the public strictly for educational and academic research purposes. The Website relies upon publicly available data from multiple sources, that do not always agree. [KawalCOVID19.id](http://kawalcovid19.id) hereby disclaims any and all representations and warranties with respect to the Website, including accuracy, fitness for use, and merchantability. Reliance on the Website for medical guidance or use of the Website in commerce is strictly prohibited.

## Copyright
[KawalCOVID19.id](http://kawalcovid19.id) disebarluaskan di bawah [Lisensi Creative Commons Atribusi-NonKomersial-TanpaTurunan 4.0 Internasional](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.id)